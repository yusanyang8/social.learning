## SOCIAL LEARNING PROJECT

---

This package is a repository of data and analyses for the social learning project done at Washington University in St. Louis 2020-2022

### Description of project

Animals often acquire knowledge by observing other individuals in their social group. Social learning allows individuals to adopt advantageous behavioral strategies by copying other successful individuals, and can also facilitate the spread of adaptive behaviors in a population. 

Trinidadian guppies (Poecilia reticulata) are known for their repeated, rapid adaptation to high- and low-predation environments (HP and LP, hereafter). LP guppies are characterized by choosier females, colorful and showy males, and reduced male harassment compared to HP guppies. The divergent male mating strategies adopted by HP and LP guppies have a genetic basis, but are also influenced by developmental exposure to social and predator cues.

Here, we tested the hypothesis that male and female mating strategies are shaped by adaptive social learning. We reared HP and LP juveniles with either HP or LP adult tutors or on their own, in environments with or without predator cues (a 2 x 3 x 2 factorial design). We assayed these males and female behaviors:

- Female preference for HP vs LP males

- Male aggression

- Male mating strategy (courtship and sneak mating)


In addition to mating behaviors, we also measured a variety of other behavioral, morphological, and life history traits, exploring how these traits are shaped by developmental plasticity in response to changes in the social and ecological environment. These include:

- Color

- Genitalia (gonopodia length)

- Brain morphology

- Growth and mature rate

- Foraging behavior




### Collaboration
(modified from Andrés Lopez-Sepulcre)

If you are a member of the project but just wants to see how the analyses were made or reports of the results, you can view the code and results here: https://yusanyang8.gitlab.io/social.learning/index.html


If you want to use the package or need to contribute data, documentation, or code you must clone this package and install it in your computer.

This project is version controlled via `git` hosted in [GitLab](https://gitlab.com/) to enable sharing and synchronization among collaborators.

In order to have a copy of this package in your computer, you must:

- Ask for access rights to the GitLab project repository at: https://gitlab.com/yusanyang8/social.learning

- Have `git` installed and set up in your computer. If you don't, go to  https://git-scm.com to download it.

- If you never used git collaboratively, you will probably have to create an SSH key as described [here](http://happygitwithr.com/ssh-keys.html).


To clone the full project repository, type in Terminal:

```
git clone git@gitlab.com:yusanyang8/social.learning.git
```
 
To contribute, you can then open the package with RStudio by clicking on the `social.learning.Rproj` file. Remember to branch, commit and request merges with your code.

To use the package, simply type in RStudio console:

```
library(social.learning)
```


