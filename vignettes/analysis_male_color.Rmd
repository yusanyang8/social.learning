---
title: "Analysis on male color"
author: "Yusan Yang"
date: "2023-09-15"
output: html_document
---

```{r, include=FALSE}
library(knitr)
  opts_chunk$set(echo = TRUE, warning = F)

library(psych)
library(tidyverse)
library(car)
library(lme4)
library(ggpubr)
library(emmeans)
library(GGally) #for scatter plots

```

```{r, include = FALSE}
load(here::here('data/data_male_color.Rda'))
```

## Preliminaries

### sample size
```{r}
data_color_merged %>%
  select(pop, pred_trt, tutor_pop) %>%
  ftable() 
```

### Data processing for plotting

```{r}
#prep dataset for facet wrap
data_long <- data_color_merged %>%

  select("pop","tutor_pop","pred_trt",
         
         "body_black_num", "body_black_area", "body_black_prop",
         "body_fuzzyblack_num", "body_fuzzyblack_area","body_fuzzyblack_prop",
         "body_melanin_num", "body_melanin_area","body_melanin_prop",
         "body_yellow_num", "body_yellow_area","body_yellow_prop",
         "body_orange_num", "body_orange_area","body_orange_prop",
         "body_xanthophore_num", "body_xanthophore_area", "body_xanthophore_prop",
         
         "tail_black_num", "tail_black_area", "tail_black_prop",
         "tail_fuzzyblack_num", "tail_fuzzyblack_area","tail_fuzzyblack_prop",
         "tail_melanin_num", "tail_melanin_area","tail_melanin_prop",
         "tail_yellow_num", "tail_yellow_area","tail_yellow_prop",
         "tail_orange_num", "tail_orange_area","tail_orange_prop",
         "tail_xanthophore_num", "tail_xanthophore_area", "tail_xanthophore_prop",
         
         "body_area","tail_area",'gono_resid') %>%
  
  pivot_longer(-c("pop", "tutor_pop", "pred_trt"),
               names_to = "variable",
               values_to = "value") %>%
  
  group_by(variable) %>%

  #reorder for neater plotting
  mutate(variable = as.factor(variable)) %>%
  mutate(variable = fct_relevel(variable,
         "body_black_num", "body_black_area", "body_black_prop",
         "body_fuzzyblack_num", "body_fuzzyblack_area","body_fuzzyblack_prop",
         "body_melanin_num", "body_melanin_area","body_melanin_prop",
         "body_yellow_num", "body_yellow_area","body_yellow_prop",
         "body_orange_num", "body_orange_area","body_orange_prop",
         "body_xanthophore_num", "body_xanthophore_area", "body_xanthophore_prop",
         
         "tail_black_num", "tail_black_area", "tail_black_prop",
         "tail_fuzzyblack_num", "tail_fuzzyblack_area","tail_fuzzyblack_prop",
         "tail_melanin_num", "tail_melanin_area","tail_melanin_prop",
         "tail_yellow_num", "tail_yellow_area","tail_yellow_prop",
         "tail_orange_num", "tail_orange_area","tail_orange_prop",
         "tail_xanthophore_num", "tail_xanthophore_area", "tail_xanthophore_prop",
         
         "body_area","tail_area",'gono_resid'))
```

## Exploratory Figures

### HP vs LP, pooling all treatments
```{r, fig.width=9, fig.height=25}
ggplot(data_long, aes(x =pop,fill = pop, y = value)) + 
  geom_boxplot() + facet_wrap(variable ~., scales = 'free', ncol = 3) +
    theme_bw() + theme(panel.grid.major = element_blank(),
                       panel.grid.minor = element_blank()) +
    scale_fill_manual(values=c("steelblue", "lightsalmon1" ))
```


### HP, plot by predator and social treatment
```{r, , fig.width=9, fig.height=25}
ggplot(data_long %>% filter(pop=="AH"), 
       aes(x =tutor_pop ,fill = pred_trt, y = value)) + 
  geom_boxplot() + facet_wrap(variable ~., scales = 'free', ncol = 3) +
    theme_bw() + theme(panel.grid.major = element_blank(),
                       panel.grid.minor = element_blank()) +
    scale_fill_manual(values=c("lightsalmon1", "steelblue" ))
```






### LP, plot by predator and social treatment
```{r, , fig.width=9, fig.height=25}
ggplot(data_long %>% filter(pop=="AL"), 
       aes(x =tutor_pop ,fill = pred_trt, y = value)) + 
  geom_boxplot() + facet_wrap(variable ~., scales = 'free', ncol = 3) +
    theme_bw() + theme(panel.grid.major = element_blank(),
                       panel.grid.minor = element_blank()) +
    scale_fill_manual(values=c("lightsalmon1", "steelblue" ))
```

### HP vs LP, listing all treatments

```{r, fig.width=13, fig.height=25}
ggplot(data_long,
       aes(x =tutor_pop:pred_trt ,fill = pop, y = value)) + 
  geom_boxplot() + facet_wrap(variable ~., scales = 'free', ncol = 3) +
    theme_bw() + theme(panel.grid.major = element_blank(),
                       panel.grid.minor = element_blank(),
                       legend.position = "top") +
    scale_fill_manual(values=c("steelblue","lightsalmon1" )) 
```

### Correlation among colors, gonopodia, behavior, and brain

#### Exploratory scatterplots

Stats in this preliminary plot are based on Gaussian correlations with no random effects so could be different from the GLMM models but should show the same trends


```{r, warning=FALSE, message = FALSE, fig.width=12, fig.height=12}
scatterplot<- 
  ggpairs(data_color_merged, 
        columns = c("gono_resid", "BM_resid", "sigmoid_prop",
                    "body_black_prop", 'tail_black_prop', 'body_orange_prop', 'tail_orange_prop'), 
        aes(color = pop),
        title = "Scatterplots & Correlation matrix") +
  scale_color_manual(values = c("SteelBlue", "lightsalmon1"))+
  scale_fill_manual(values = c("SteelBlue", "lightsalmon1"))+
  theme_bw(base_size = 15)

print(scatterplot)
```



```{r, warning=FALSE, message = FALSE, fig.width=12, fig.height=12}
# Create a function for binned scatter plots
binned_scatter <- function(data, mapping, bins = 8, ...){
  ggplot(data = data, mapping = mapping) +
    
    stat_summary_bin(
                   fun.data = mean_se, 
                   bins=8, size =0.5, position = position_dodge(0.02))+

    theme_bw(base_size = 15)  
}

# Create the scatter plot matrix with binned scatter plots
scatterplot <- ggpairs(data_color_merged, 
                      columns = c("gono_resid", "BM_resid", "sigmoid_prop",
                                  "body_black_prop", 'tail_black_prop', 'body_orange_prop', 'tail_orange_prop'), 
                      aes(color = pop),
                      title = "Binned Scatterplots & Correlation matrix",
                      lower = list(continuous = binned_scatter))+
  scale_color_manual(values = c("SteelBlue", "lightsalmon1"))+
  scale_fill_manual(values = c("SteelBlue", "lightsalmon1"))

print(scatterplot)
```


```{r, warning=FALSE, message = FALSE, fig.width=12, fig.height=12}
# Create a function for binned scatter plots
binned_scatter <- function(data, mapping, bins = 8, ...){
  ggplot(data = data, mapping = mapping) +
    
    stat_summary_bin(
                   fun.data = mean_se, 
                   bins=8, size =0.5, position = position_dodge(0.02))+

    theme_bw(base_size = 15)  
}

# Create the scatter plot matrix with binned scatter plots
scatterplot <- ggpairs(data_color_merged %>% filter(tutor_pop=="solo"), 
                      columns = c("gono_resid", "BM_resid", "sigmoid_prop",
                                  "body_black_prop", 'tail_black_prop', 'body_orange_prop', 'tail_orange_prop'), 
                      aes(color = pop),
                      title = "Binned Scatterplots & Correlation matrix",
                      lower = list(continuous = binned_scatter))+
  scale_color_manual(values = c("SteelBlue", "lightsalmon1"))+
  scale_fill_manual(values = c("SteelBlue", "lightsalmon1"))

print(scatterplot)
```





#### Color and gonopodia specifically

```{r}
data_cor <- data_color_merged %>%
  gather(variable, value,
         body_melanin_prop, tail_melanin_prop, body_xanthophore_prop, tail_xanthophore_prop)
```

```{r, fig.width=7, fig.height=6}
ggplot(data_cor, aes(x = value, y = gono_resid, color = pop, fill = pop)) + 
  geom_point(pch = 21, color = "black") + 
  geom_smooth(method = lm, alpha = 0.2) +
  stat_cor(method = "pearson") +
  theme_bw(base_size = 15) + 
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        aspect.ratio = 1,
        legend.position = "none") +
  scale_fill_manual(values = c("SteelBlue", "lightsalmon1")) +
  scale_color_manual(values = c("SteelBlue", "lightsalmon1")) +
  facet_wrap(~variable, nrow = 2)
```





## Analysis

Strategy: start with pop*(pred_trt + tutor_pop) and drop when non-significant

### Body black
Both HP and LP males responded to social treatment. Body black solo > LP~HP

```{r}
mod <- lmer(logit(body_black_prop) ~ pop+pred_trt + tutor_pop+ (1|mom_ID),
            data = data_color_merged)

summary(mod)$coefficient %>% kable(digits = 3)
Anova(mod)
```

```{r}
emmeans(mod, ~ tutor_pop) %>% pairs() %>% kable(digits=3)
```

### Tail black

trend of pred+ having less tail black

```{r}
mod <- lmer(car::logit(tail_black_prop) ~ pop+pred_trt + tutor_pop+ (1|mom_ID),
            data = data_color_merged)

summary(mod)$coefficient %>% kable(digits = 3)
```



### Body melanin

LP overall have less body melanin compare to HP males
They showed trend of responding differently to predator treatment; when reared in pred+, HP decreased melanin and LP increased.

```{r}
mod <- lmer(logit(body_melanin_prop) ~ pop*pred_trt + tutor_pop+ (1|mom_ID),
            data = data_color_merged)

summary(mod)$coefficient %>% kable(digits = 3)

```

```{r}
emmeans(mod, ~ pred_trt|pop) %>% pairs() %>% kable(digits=3)
```


### Tail melanin

Nothing

```{r}
mod <- lmer(car::logit(tail_melanin_prop) ~ pop+pred_trt+tutor_pop+ (1|mom_ID),
            data = data_color_merged)

summary(mod)$coefficient %>% kable(digits = 3)

```

### Body orange

Only LP fish is sensitive to social treatment; when reared alone they have more orange

```{r}
mod <- lmer(car::logit(body_orange_prop) ~ pop*tutor_pop+ pred_trt+ (1|mom_ID),
            data = data_color_merged)

summary(mod)$coefficient %>% kable(digits = 3)
```

```{r}
emmeans(mod, ~ tutor_pop|pop) %>% pairs() %>% kable(digits=3)
```


### Tail orange

Both HP and LP responded to social treatment; solo < HP ~ LP

```{r}
mod <- lmer(car::logit(tail_orange_prop) ~ pop+tutor_pop+ pred_trt+ (1|mom_ID),
            data = data_color_merged)

summary(mod)$coefficient %>% kable(digits = 3)
```

```{r}
emmeans(mod, ~ tutor_pop) %>% pairs() %>% kable(digits=3)
```



### Body xantho

Population differ in response to tutor treatment; only AL is sensitive-ish, not significant.
```{r}
mod <- lmer(car::logit(body_xanthophore_prop) ~ pop*tutor_pop+ pred_trt+ (1|mom_ID),
            data = data_color_merged)

summary(mod)$coefficient %>% kable(digits = 3)
Anova(mod)
```

```{r}
emmeans(mod, ~tutor_pop|pop) %>% pairs %>% kable(digits=3)
```


### Tail xantho
This ones pretty interesting.
1. Both HP and LP males are influenced by social treatment; when raised with AL tutor, they develop more xantho on tail (marginal)
2. Only HP is sensitive to pred treatment; they develop more xantho in predator water.

```{r}
mod <- lmer(car::logit(tail_xanthophore_prop) ~ pop*pred_trt + tutor_pop+ (1|mom_ID),
            data = data_color_merged)

summary(mod)$coefficient %>% kable(digits = 3)
Anova(mod, type =3)
```

```{r}
emmeans(mod, ~tutor_pop) %>% pairs %>% kable(digits=3)
```

```{r}
emmeans(mod, ~pred_trt|pop) %>% pairs %>% kable(digits=3)
```












### Gonopodium length among treatments


#### solo vs HP tutor vs LP tutor
Trend of solo fish having relatively shorter gonopodia, but not significant

```{r}
mod <- lmer(gono_length ~ pop+pred_trt+tutor_pop+ length + (1|mom_ID) ,
            data = data_color_merged)

Anova(mod, type = 3)
```

```{r}
emmeans(mod, ~tutor_pop) %>% pairs %>% kable(digits=3)
```

#### solo vs tutor
Significant went grouping the two tutored groups together.
```{r}
mod <- lmer(gono_length ~ pop+pred_trt+tutor+ length + (1|mom_ID) ,
            data = data_color_merged)

Anova(mod, type = 3)
```

```{r}
emmeans(mod, ~tutor) %>% pairs %>% kable(digits=3)
```





### Correlation among gonopodia, color, brain, and behavior


#### gonopodium - behavior

Relative gonopodia length is negatively correlated with sigmoid proportion

```{r}
# ID is added as OLRE to help with overdispersion
mod <-glmer(cbind(sigmoid, sneak)  ~ gono_resid+pop+
              (1|ID)+(1|mom_ID), 
            data = data_color_merged,
            family = binomial())
summary(mod)$coefficients %>% kable(digit = 3)
```

```{r}
mod.fit <- emmip(mod, pop~gono_resid, cov.reduce = range, type = "response", plotit = F)

gono_behav <- ggplot(data = data_color_merged)+
  geom_point(aes(x = gono_resid, y = sigmoid_prop, color = pop), alpha = 0.3) +

  stat_summary_bin(aes(x = gono_resid, y = sigmoid_prop, color = pop),
                   fun.data = mean_se, 
                   bins=8, size =0.5, position = position_dodge(0.02))+
  
  geom_line(size = 1 ,data = mod.fit, aes(x = xvar, y = yvar, color = pop))+
  labs( x = "Residual gonopodium length", y = "Sigmoid proportion") +
  
  scale_color_manual("Population origin",
                     labels = c("HP","LP"),
                     values=c("SteelBlue", "lightsalmon1")) +
  
  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        legend.position = "bottom",
        aspect.ratio=1) 

gono_behav
```

#### brain size - behavior

Marginal: HP males that have larger brain sigmoids proportionally more

```{r}
mod <-glmer(cbind(sigmoid, sneak)  ~ pop*BM_resid+
              (1|ID) + (1|mom_ID), 
            data = data_color_merged,
            family = binomial(),
            control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=100000)))
summary(mod)$coefficients %>% kable(digit = 3)
```

```{r}
emtrends(mod, ~pop, "BM_resid") %>% test
```

```{r}
mod.fit <- emmip(mod, pop~BM_resid, cov.reduce = range, type = "response", plotit = F)

BM_behav <- ggplot(data = data_color_merged)+
  geom_point(aes(x = BM_resid, y = sigmoid_prop, color = pop), alpha = 0.3) +
  geom_line(size = 1 ,data = mod.fit, aes(x = xvar, y = yvar, color = pop))+
  
  stat_summary_bin(aes(x = BM_resid, y = sigmoid_prop, color = pop),
                   fun.data = mean_se, 
                   bins=8, size =0.5, position = position_dodge(0.025))+
  
  labs( x = "Residual brain mass", y = "Sigmoid proportion") +
  
  scale_color_manual("Population origin",
                     labels = c("HP","LP"),
                     values=c("SteelBlue", "lightsalmon1")) +
  scale_x_continuous(limits =c(-0.2506015, 0.4115183)) +
  
  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        legend.position = "bottom",
        aspect.ratio=1) 

BM_behav
```



#### brain size-gonopodium
Marginal: males with longer gonopodia have smaller brains

```{r}

mod <-lmer(scale(BM)  ~ gono_resid+pop + weight +
              (1|mom_ID), 
            data = data_color_merged )

summary(mod)$coefficients %>% kable(digit = 4)
Anova(mod)
```

```{r}
mod <-lmer(BM_resid  ~ gono_resid+pop +  (1|mom_ID), 
            data = data_color_merged)
mod.fit <- emmip(mod, pop~gono_resid, cov.reduce = range, type = "response", plotit = F)

BM_gono <- ggplot(data = data_color_merged)+
  geom_point(aes(x = gono_resid, y = BM_resid, color = pop), alpha = 0.3) +
  geom_line(size = 1 ,data = mod.fit, aes(x = xvar, y = yvar, color = pop))+
  
  stat_summary_bin(aes(x = gono_resid, y = BM_resid, color = pop),
                   fun.data = mean_se, 
                   bins=8, size =0.5, position = position_dodge(0.025))+
  
  labs( x = "Residual gonopodium length", y = "Residual brain mass") +
  
  scale_color_manual("Population origin",
                     labels = c("HP","LP"),
                     values=c("SteelBlue", "lightsalmon1")) +
  
  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        legend.position = "bottom",
        aspect.ratio=1) 

BM_gono

```


#### Color

Didn't run yet - pending discussion on what exactly we want to run.



#### Multipanel figure (add more graphs as needed)

```{r, fig.width=11, fig.height=4}
egg::ggarrange(gono_behav + theme(legend.position = "none"),
               BM_behav, 
               BM_gono + theme(legend.position = "none"), nrow = 1,labels = c("A","B","C"))
```





## Variable key

**1. Treatment variables & fish info:**

* `ID`: unique ID of the fish
* `flag`: highlighted during analysis, y = uncertainty about ID handwriting, c = corrected when merging
* `pop`: fish origin; Aripo HP (AH) / Aripo LP(AL)
* `pred_trt`: Rearing water treatment; with or without predator chemical cues (Y/N)            
* `tutor_pop`: Social treatment; reared with AH tutors, AL tutors, or by itself (AH/AL/solo)
* `block`: Experiment block; fish in the same block are processed on the same day
* `mom_ID`: the focal's mom ID; fish with the same mom_ID are full siblings
* `clutch_num`: the number of the clutch the focal fish is from
* `sex`: Focal fish sex; male or female (M/F); all M in this dataset    
* `tutor_ID`: ID of the tutors

<p>&nbsp;</p>

**2. Life history and morphology data:**

* `birth`: date of birth
* `mature_date`: date of reaching sexual maturity (based on gonopodium hood; checked every week)
* `mature_lat`: latency (days) to sexual maturity          
* `assay_date`: date of behavioral assay
* `age`: age (days) when assayed
* `weight`: weight (g) when assayed
* `length`: body length (mm) when assayed
* `gono_length`: gonopodium length (mm)         
* `eye_width`: eye width(mm)
* `body_width`: body width (mm)
* `tail_width`: tail width (mm)
* `BM`: Brain mass (g)
* `Cb`: Cerebellum volume (mm^3)
* `OT`: Optic Tectum volume (mm^3)
* `Tel`: Telencephalon volume (mm^3)
* `OB`: Olfactory bulb volume (mm^3)
* `Hyp`: Hypothalamus volume (mm^3)
* `BM_resid`: Brain mass residual against body mass
* `Cb_resid`: Cerebellum volume against brain mass
* `OT_resid`: Optic Tectum volume against brain mass
* `Tel_resid`: Telencephalon volume against brain mass
* `OB_resid`: Olfactory bulb volume against brain mass
* `Hyp_resid`: Hypothalamus volume against brain mass

   
<p>&nbsp;</p>

**3. Color data:**

* `body_length`: ImageJ measured body length (mm)
* `body_area`: body area (does not include tail) (mm^2)
* `body_black_num`: number of black spots
* `body_black_area`: total area of black spots       
* `body_black_prop`: `body_black_area` / `body_area`    
* `body_fuzzyblack_num`: number of fuzzy black spots
* `body_fuzzyblack_area`: total area of fuzzy black spots         
* `body_fuzzyblack_prop`: `body_fuzzyblack_area` / `body_area`     
* `body_melanin_num`: `body_black_num` + `body_fuzzyblack_num`      
* `body_melanin_area`: `body_black_area` + `body_fuzzyblack_area`     
* `body_melanin_prop`: `body_melanin_area`/ `body_area`
* `body_yellow_num`: number of yellow orange spots       
* `body_yellow_area`: total area of yellow orange spots      
* `body_yellow_prop`: `body_yellow_area`/`body_area`
* `body_orange_num`: number of orange spots       
* `body_orange_area`: total area of orange spots      
* `body_orange_prop`: `body_orange_area`/`body_area`     
* `body_xanthophore_num`: `body_yellow_num`+`body_orange_num`  
* `body_xanthophore_area`: `body_yellow_area`+`body_orange_area` 
* `body_xanthophore_prop`: `body_xanthophore_area`/`body_area` 
* `tail_area`: tail area (mm^2)             
* `tail_black_num`: total number of black spots on tail
* `tail_black_area`: total area of black spots on tail       
* `tail_black_prop`: `tail_black_area`/`tail_area`       
* `tail_fuzzyblack_num`: total number of fuzzy black spots on tail    
* `tail_fuzzyblack_area`: total area of fuzzy black spots on tail      
* `tail_fuzzyblack_prop`: `tail_fuzzyblack_area`/`tail_area`  
* `tail_melanin_num`: `tail_black_num` + `tail_fuzzyblack_num`   
* `tail_melanin_area`: `tail_black_area` + `tail_fuzzyblack_area`   
* `tail_melanin_prop`:  `tail_melanin_area`/`tail_area`
* `tail_yellow_num`: total number of yellow orange spots on tail       
* `tail_yellow_area`: total area of yellow orange spots on tail        
* `tail_yellow_prop`: `tail_yellow_area`/`tail_area`      
* `tail_orange_num`: total number of orange spots on tail       
* `tail_orange_area`: total number of orange area on tail      
* `tail_orange_prop`: `tail_orange_area`/`tail_area`       
* `tail_xanthophore_num`: `tail_yellow_num` + `tail_orange_num` 
* `tail_xanthophore_area`: `tail_yellow_area` + `tail_orange_area`
* `tail_xanthophore_prop`: `tail_xanthophore_area`/`tail_area`


