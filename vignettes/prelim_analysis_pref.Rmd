
---
title: 'Preliminary analysis: female preference'
output:
  html_document:
    code_folding: hide
---

```{r, include=FALSE}
library(knitr)
  opts_chunk$set(echo = TRUE, warning = FALSE, message = FALSE)
library(tidyverse)
library(lme4)
library(lmerTest)
```


## Prep dataset

Import data 

```{r, warning = FALSE}
data <- read.csv(here::here("data_raw/FemalePref_041822.csv"),fileEncoding="UTF-8-BOM")

```


Data clean up

```{r}

data <- data %>% 
  #proportional time spent with HP
  mutate(HP_pref = Total.time.HP/(Total.time.HP	+Total.time.LP)) %>%
  
  #Time spent with either male
  mutate(responsiveness = (Total.time.HP	+Total.time.LP)/Total.time)
  

```


subset data

```{r}
data_solo <- data %>% filter(soc_trt=="solo") %>% droplevels()
data_soc <- data %>% filter(soc_trt !="solo") %>% droplevels()
```


sample size

```{r}
data_solo %>% select(pop, pred_trt) %>% table()/2
```

```{r}
data_soc %>% select(pred_trt, pop, soc_trt) %>% table()/2
```

## Solo dataset exploratory


### Preference

Preliminary results show that female preference is different between HP and LP fish, and the preference is dependent upon the ecological and social environment they grey up in.

AH females showed preference for males from their own population when they are reared in water with predator cues, but not when they are reared in regular water. In contrast, AL females showed preference for males from their own population when they are reared in regular water, but not in water with predator cues.


```{r}
ggplot(data_solo, 
       aes(x = pred_trt, y = HP_pref, fill = pop)) +
  geom_boxplot()+
  geom_point(position=position_jitterdodge(jitter.width = 0.1)) +
  geom_hline(yintercept=0.5, linetype="dashed")+
  
  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```


### Responsiveness

Moreover, females are in general more responsive to males when they were reared in regular water as compared to in predator water.

```{r}
ggplot(data_solo, 
       aes(x = pred_trt, y = responsiveness, fill = pop)) +
  geom_boxplot()+
  geom_point(position=position_jitterdodge(jitter.width = 0.1)) +

  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```





## Soc dataset exploratory

Preliminary results show that female preferences in HP and LP fish is jointly influenced by their population origin, the social environment, and the ecological environment.


### Preference

HP fish preferred HP males when they were reared with HP tutors, but prefer LP males when they were reared with LP tutors, regardless of the rearing water type.

#### AH
```{r}
ggplot(data_soc %>% filter(pop=="AH"), 
       aes(x = pred_trt, y = HP_pref, fill = soc_trt)) +
  geom_boxplot()+
  geom_point(position=position_jitterdodge(jitter.width = 0.1)) +
  geom_hline(yintercept=0.5, linetype="dashed")+
  
  
  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```

In contrast, the preference of AL females are not influenced by the social environment. They show a trend of preferring HP males when they are reared in regular water, and no preference when reared in water with predator cues.

#### AL
```{r}
ggplot(data_soc %>% filter(pop=="AL"), 
       aes(x = pred_trt, y = HP_pref, fill = soc_trt)) +
  geom_boxplot()+
  geom_point(position=position_jitterdodge(jitter.width = 0.1)) +
  geom_hline(yintercept=0.5, linetype="dashed")+
  
  
  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```

### Responsiveness

For both populations, females reared with AH tutors are more responsive compared to females reared with AL tutors.


#### AH
```{r}
ggplot(data_soc %>% filter(pop=="AH"), 
       aes(x = pred_trt, y = responsiveness, fill = soc_trt)) +
  geom_boxplot()+
  geom_point(position=position_jitterdodge(jitter.width = 0.1)) +

  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```

#### AL
```{r}
ggplot(data_soc %>% filter(pop=="AL"), 
       aes(x = pred_trt, y = responsiveness, fill = soc_trt)) +
  geom_boxplot()+
  geom_point(position=position_jitterdodge(jitter.width = 0.1)) +

  theme_bw(base_size = 15)+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```

